
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дъмп структура за таблица file_system_app.file_counter
CREATE TABLE IF NOT EXISTS `file_counter` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица file_system_app.file_counter: ~0 rows (приблизително)
/*!40000 ALTER TABLE `file_counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_counter` ENABLE KEYS */;

-- Дъмп структура за таблица file_system_app.system_state
CREATE TABLE IF NOT EXISTS `system_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tree_capacity` int(11) NOT NULL,
  `proportion_system` int(11) NOT NULL,
  `proportion_dir` int(11) NOT NULL,
  `proportion_growth_factor` int(11) NOT NULL,
  `dir_capacity` int(11) NOT NULL,
  `dir_growth_factor` int(11) NOT NULL,
  `dir_state` tinyint(1) NOT NULL,
  `date_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица file_system_app.system_state: ~0 rows (приблизително)
/*!40000 ALTER TABLE `system_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_state` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
