This is a system implemented with Binary Balanced Search Tree of directories.


What the system can do?

- Store filed in 3 places: Archive, Extensions and tree structure.
- Re-size and re-fill when tree capacity is reached
- Search and download files


What are the tree system properties?

- Capacity of the tree
- Capacity of a single folder
- Proportion of folders/folder capacity. Example 5:1 means we are having 5 times more
folders than a single folder capacity. If one folder capacity is 3 files then we will have 15
folders in the tree which means that every 15 folders can hold 3 files and the capacity of
the whole tree is 45 files.
- Growth factor for the proportion
- Growth factor for the directory capacity
- Folders capacity could be Fixed and Unfixed. If fixed the capacity of the folder stays the
same when tree resize.
- *growth factors are used when tree is resizing due to reaching its capacity


What the application can do in terms of changing the tree properties?

- Calculating potential system capacity
- Changing the system state - tree proportion, growth factors, directory capacity,
directory state (fixed/unfixed), directory/file proportions. These operations are possible
at any time. No matter how full or empty the tree structure is. What is happening when
we decide to change the tree system state? We add new state and re-build and re-fill
the new tree structure and delete the old ones.
- Also the application shows us the actual system state and what will be the next one in
terms of capacity, proportion and directory properties.


Why do we need to be able to change tree settings?

Because this way we can setup a tree that fits a specific need. For Example: we may need a
system where we hold only one file (for fast search) in a directory, or any other number. We
may want a system with higher or exact total file capacity. In general, its much better for us to
be able to control the tree structure properties and setup instead of having a fixed one.


How do we store a file in the tree?

Every tree directory has and ID. Every new file gets new ID too. Once we know the new file ID
and directory capacity, we are generating and URL address where in the tree is the directory
where we have to copy this file. This function is not related to file listing, it generates only a
string without traversing the tree and searching the folder.


How do we search a file in a tree?

It is the same as above. We use the same function to generate the URL to the directory with
files where the wanted one is. Once we have the exact directory then we list all files from it and
iterate trough them until we find the one with the wanted ID. Example: Suppose we have 5 files
in a directory, the application will generate the path to the directory, list the 5 files in it, search
the one and return its absolute path.
The most important about these two operations is that we DO NOT list all files in a tree, only
the files in the directory where our file is.


What more functionalities are planned?

- Ability to set a percentage for the tree to re-size and re-fill.
- Ability to set up an automatic mechanism controlling how often a tree is re-sizing and
re-building, due to too may or too little files being uploaded. Why? Because re-building
and re-filling a tree is time consuming and if it happens often the system will be slow.
- More information and simulations according to future tree growth based on how often
and or when files are uploaded. 






