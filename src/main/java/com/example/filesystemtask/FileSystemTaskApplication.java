package com.example.filesystemtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileSystemTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileSystemTaskApplication.class, args);
    }

}
