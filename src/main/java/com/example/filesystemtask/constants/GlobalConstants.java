package com.example.filesystemtask.constants;

import java.time.LocalDateTime;

public class GlobalConstants {

    //DEFAULT FILE SYSTEM SETUP

    public static final int DIRECTORY_CAPACITY = 5;

    public static final int PROPORTION_SYSTEM = 1;

    public static final int PROPORTION_DIRECTORY = 1;

    public static final int PROPORTION_GROWTH_FACTOR = 1;

    public static final int INITIAL_TREE_CAPACITY = DIRECTORY_CAPACITY * (DIRECTORY_CAPACITY * PROPORTION_SYSTEM);

    public static final int DIRECTORY_CAPACITY_GROWTH_FACTOR = 1;

    // false = UNFIXED - (dir.cap increase)
    // true = FIXED - (dir.cap does NOT increase)
    public static final boolean DIRECTORY_STATE_FIXED = false;

    public static final LocalDateTime DATE_OF_CREATION = LocalDateTime.now();


    //FILE SYSTEM DIRECTORIES SETUP

    //here we can configure root directory
    public static final String ROOT_DIRECTORY = "C:\\JAVA Stuff\\FileSystemRepo\\file-storage-system\\";

    public static final String TREE = "tree\\";

    public static final String ARCHIVE = "archive\\";

    public static final String EXTENSIONS = "extensions\\";

    public static final String DIRECTORY_CONTAINER = "directory-container\\";

    public static final String TREE_DIRECTORY = ROOT_DIRECTORY + DIRECTORY_CONTAINER + TREE;

    public static final String ARCHIVE_DIRECTORY = ROOT_DIRECTORY + DIRECTORY_CONTAINER +  ARCHIVE;

    public static final String EXTENSIONS_DIRECTORY = ROOT_DIRECTORY + DIRECTORY_CONTAINER + EXTENSIONS;


    //OTHER CONSTANTS

    public static final String CONTAINS_NON_DIGIT_SYMBOLS = "Id number contains non digit symbols.";

    public static final String NO_CAPACITY_FACTORS = "No capacity factors in database.";

    public static final String NO_SYSTEM_STATES = "No system-state registered in database.";

    public static final String OPEN_TAG = "#_";

    public static final String CLOSE_TAG = "_#_";

    public static final String DASH = "-";

    public static final char DASH_CHAR = '-';

    public static final char UNDER_LINE = '_';

    public static final char HASHTAG = '#';

    public static final char BACKSLASH = '\\';

    public static final char DOT = '.';

    public static final int ASCII_ZERO = 48;

    public static final int ASCII_NINE = 57;

}
