package com.example.filesystemtask.controllers.mvc;

import com.example.filesystemtask.exceptions.EntityNotFoundException;
import com.example.filesystemtask.models.SearchFileDto;
import com.example.filesystemtask.services.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
@RequestMapping("/download")
public class DownloadFileControllerMvc {

    final private FileStorageService fileStorageService;

    @Autowired
    public DownloadFileControllerMvc(FileStorageService fileStorageService) {
        this.fileStorageService = fileStorageService;
    }

    @GetMapping
    public String showFileDownload(Model model) {

        model.addAttribute("searchFileDto", new SearchFileDto());
        return "download";
    }


    @PostMapping
    public String handleFileDownload(HttpServletResponse response,
                                     @Valid @ModelAttribute("searchFileDto") SearchFileDto fileDto,
                                     Model model) {

        try {
            Path file = Paths.get(fileStorageService.getFilePathById(fileDto.getFileId()));

            if (Files.exists(file)) {
                response.setContentType("application/pdf");
                response.addHeader("Content-Disposition", "attachment; filename=" + file);

                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            }

        } catch (EntityNotFoundException | IOException e) {
            model.addAttribute("exceptionsMessages", e.getMessage());
            return "download";
        }

        return "download";
    }
}
