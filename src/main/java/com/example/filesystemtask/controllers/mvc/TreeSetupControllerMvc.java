package com.example.filesystemtask.controllers.mvc;

import com.example.filesystemtask.models.SystemState;
import com.example.filesystemtask.models.dto.CalculateSystemStateDto;
import com.example.filesystemtask.models.dto.SystemStateDto;
import com.example.filesystemtask.models.mappers.SystemSetupMapper;
import com.example.filesystemtask.repositories.contracts.FileCountRepository;
import com.example.filesystemtask.repositories.contracts.SystemStateRepository;
import com.example.filesystemtask.services.SystemStateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/tree-setup")
public class TreeSetupControllerMvc {

    private final SystemStateService systemStateService;
    private final SystemSetupMapper systemSetupMapper;
    private final SystemStateRepository systemStateRepository;
    private final FileCountRepository fileCountRepository;

    @Autowired
    public TreeSetupControllerMvc(SystemStateService systemStateService,
                                  SystemSetupMapper systemSetupMapper,
                                  SystemStateRepository systemStateRepository,
                                  FileCountRepository fileCountRepository) {
        this.systemStateService = systemStateService;
        this.systemSetupMapper = systemSetupMapper;
        this.systemStateRepository = systemStateRepository;
        this.fileCountRepository = fileCountRepository;
    }

    @ModelAttribute("currentSystemState")
    public SystemState populateSystem() {
        return systemStateRepository.getSystemState();
    }

    @ModelAttribute("filesCount")
    public int populateFileCount() {
        return fileCountRepository.getLastFileCount();
    }

    @GetMapping
    public String showTreeSetupPage(Model model){

        model.addAttribute("calculateSystemStateDto", new CalculateSystemStateDto());
        model.addAttribute("systemStateDto", new SystemStateDto());
        return "tree-setup";
    }

    @PostMapping("/calculate")
    public String calculateTreeSetup(
            @Valid @ModelAttribute("calculateSystemStateDto") CalculateSystemStateDto dto,
            HttpSession session) {

        String systemState = systemStateService.calculate(dto);
        session.setAttribute("systemStateCalculated", systemState);

        return "redirect:/tree-setup";
    }

    @PostMapping("/change")
    public String changeTreeSetup(
            @Valid @ModelAttribute("systemStateDto") SystemStateDto dto,
            Model model) {

        SystemState newSystemState = systemSetupMapper.fromDto(dto);
        systemStateService.changeState(newSystemState);
        model.addAttribute("systemStateChangedSuccessfully", "State changed!");

        return "redirect:/tree-setup";
    }
}
