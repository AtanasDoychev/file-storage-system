package com.example.filesystemtask.controllers.mvc;

import com.example.filesystemtask.exceptions.FileOperationsException;
import com.example.filesystemtask.exceptions.DirectoryBuildException;
import com.example.filesystemtask.exceptions.EntityNotFoundException;
import com.example.filesystemtask.services.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/upload")
public class UploadFileControllerMvc {

    final private FileStorageService fileStorageService;

    @Autowired
    public UploadFileControllerMvc(FileStorageService fileStorageService) {
        this.fileStorageService = fileStorageService;
    }


    @GetMapping
    public String showFileUpload() {

        return "upload";
    }


    @PostMapping
    public String handleFileUpload(Model model, HttpSession session,
                                   @RequestParam("file") MultipartFile file,
                                   RedirectAttributes attributes){

        if (file.isEmpty()) {
            attributes.addFlashAttribute("message", "Please select a file to upload.");
            return "upload";
        }

        String fileRegistrationName = "";

        try {
            fileRegistrationName = fileStorageService.storeFile(file);
        } catch (EntityNotFoundException | DirectoryBuildException | FileOperationsException e) {
            model.addAttribute("exceptionsMessages", e.getMessage());
            return "upload";
        }

        session.setAttribute("newRegistrationName", fileRegistrationName);
        attributes.addFlashAttribute("message", "File have been uploaded.");

        return "upload";
    }

}
