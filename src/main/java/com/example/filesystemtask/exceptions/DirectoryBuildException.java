package com.example.filesystemtask.exceptions;

public class DirectoryBuildException extends RuntimeException {

    public DirectoryBuildException() {
    }

    public DirectoryBuildException(String message) {
        super(message);
    }
}
