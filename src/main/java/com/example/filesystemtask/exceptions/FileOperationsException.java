package com.example.filesystemtask.exceptions;

public class FileOperationsException extends RuntimeException{

    public FileOperationsException() {
    }

    public FileOperationsException(String message) {
        super(message);
    }
}
