package com.example.filesystemtask.exceptions;

public class SystemStateException extends RuntimeException{

    public SystemStateException() {
    }

    public SystemStateException(String message) {
        super(message);
    }
}
