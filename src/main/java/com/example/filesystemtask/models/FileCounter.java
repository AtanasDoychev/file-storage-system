package com.example.filesystemtask.models;

import javax.persistence.*;

@Entity
@Table(name = "file_counter")
public class FileCounter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "file_id")
    private int fileId;

    public FileCounter() {
    }

    public int getFileId() {
        return fileId;
    }

    public void setFileId(int number) {
        this.fileId = number;
    }
}
