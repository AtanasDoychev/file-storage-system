package com.example.filesystemtask.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class SearchFileDto {

    @NotNull
    @Positive
    private int fileId;

//    @Size(min = 1, message = "File name must be at least one sign long.")
//    private String fileName;


    public SearchFileDto() {
    }

    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }
}
