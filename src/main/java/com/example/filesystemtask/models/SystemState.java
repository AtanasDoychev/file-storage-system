package com.example.filesystemtask.models;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "system_state")
public class SystemState {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "tree_capacity")
    private int treeCapacity;

    @Column(name = "proportion_system")
    private int proportionSystem;

    @Column(name = "proportion_dir")
    private int proportionDir;

    @Column(name = "proportion_growth_factor")
    private int proportionGrowthFactor;

    @Column(name = "dir_capacity")
    private int dirCapacity;

    @Column(name = "dir_growth_factor")
    private int dirGrowthFactor;

    @Column(name = "dir_state")
    private boolean dirState;

    @Column(name = "date_time")
    private LocalDateTime dateTime;

    public SystemState() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTreeCapacity() {
        return treeCapacity;
    }

    public void setTreeCapacity(int treeCapacity) {
        this.treeCapacity = treeCapacity;
    }

    public int getProportionSystem() {
        return proportionSystem;
    }

    public void setProportionSystem(int proportionSystem) {
        this.proportionSystem = proportionSystem;
    }

    public int getProportionDir() {
        return proportionDir;
    }

    public void setProportionDir(int proportionDir) {
        this.proportionDir = proportionDir;
    }

    public int getProportionGrowthFactor() {
        return proportionGrowthFactor;
    }

    public void setProportionGrowthFactor(int proportionGrowthFactor) {
        this.proportionGrowthFactor = proportionGrowthFactor;
    }

    public int getDirCapacity() {
        return dirCapacity;
    }

    public void setDirCapacity(int dirCapacity) {
        this.dirCapacity = dirCapacity;
    }

    public int getDirGrowthFactor() {
        return dirGrowthFactor;
    }

    public void setDirGrowthFactor(int dirGrowthFactor) {
        this.dirGrowthFactor = dirGrowthFactor;
    }

    public boolean isDirState() {
        return dirState;
    }

    public void setDirState(boolean dirState) {
        this.dirState = dirState;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
}
