package com.example.filesystemtask.models.dto;

public class CalculateSystemStateDto {

    private long systemProportion;

    private long directoryCapacity;

    public CalculateSystemStateDto() {
    }

    public long getSystemProportion() {
        return systemProportion;
    }

    public void setSystemProportion(long systemProportion) {
        this.systemProportion = systemProportion;
    }

    public long getDirectoryCapacity() {
        return directoryCapacity;
    }

    public void setDirectoryCapacity(long directoryCapacity) {
        this.directoryCapacity = directoryCapacity;
    }
}
