package com.example.filesystemtask.models.dto;

import java.time.LocalDateTime;

public class SystemStateDto {

    private int treeCapacity;

    private int proportionSystem;

    private int proportionDir;

    private int proportionGrowthFactor;

    private int dirCapacity;

    private int dirGrowthFactor;

    private boolean dirState;

    public SystemStateDto() {
    }

    public int getTreeCapacity() {
        return treeCapacity;
    }

    public void setTreeCapacity(int treeCapacity) {
        this.treeCapacity = treeCapacity;
    }

    public int getProportionSystem() {
        return proportionSystem;
    }

    public void setProportionSystem(int proportionSystem) {
        this.proportionSystem = proportionSystem;
    }

    public int getProportionDir() {
        return proportionDir;
    }

    public void setProportionDir(int proportionDir) {
        this.proportionDir = proportionDir;
    }

    public int getProportionGrowthFactor() {
        return proportionGrowthFactor;
    }

    public void setProportionGrowthFactor(int proportionGrowthFactor) {
        this.proportionGrowthFactor = proportionGrowthFactor;
    }

    public int getDirCapacity() {
        return dirCapacity;
    }

    public void setDirCapacity(int dirCapacity) {
        this.dirCapacity = dirCapacity;
    }

    public int getDirGrowthFactor() {
        return dirGrowthFactor;
    }

    public void setDirGrowthFactor(int dirGrowthFactor) {
        this.dirGrowthFactor = dirGrowthFactor;
    }

    public boolean isDirState() {
        return dirState;
    }

    public void setDirState(boolean dirState) {
        this.dirState = dirState;
    }
}
