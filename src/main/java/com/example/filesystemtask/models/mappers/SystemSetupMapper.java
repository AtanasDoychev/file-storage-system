package com.example.filesystemtask.models.mappers;

import com.example.filesystemtask.models.SystemState;
import com.example.filesystemtask.models.dto.SystemStateDto;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import static com.example.filesystemtask.constants.GlobalConstants.PROPORTION_DIRECTORY;

@Component
public class SystemSetupMapper {

    public SystemState fromDto (SystemStateDto dto){

        SystemState newSystemState = new SystemState();

        int newTreeCapacity = (dto.getProportionSystem() * dto.getDirCapacity()) * dto.getDirCapacity();

        newSystemState.setTreeCapacity(newTreeCapacity);
        newSystemState.setProportionSystem(dto.getProportionSystem());
        newSystemState.setProportionDir(PROPORTION_DIRECTORY);
        newSystemState.setProportionGrowthFactor(dto.getProportionGrowthFactor());
        newSystemState.setDirCapacity(dto.getDirCapacity());
        newSystemState.setDirGrowthFactor(dto.getDirGrowthFactor());
        newSystemState.setDirState(dto.isDirState());
        newSystemState.setDateTime(LocalDateTime.now());

        return newSystemState;
    }
}
