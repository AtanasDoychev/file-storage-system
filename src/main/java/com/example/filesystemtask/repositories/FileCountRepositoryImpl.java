package com.example.filesystemtask.repositories;

import com.example.filesystemtask.repositories.contracts.FileCountRepository;
import com.example.filesystemtask.exceptions.EntityNotFoundException;
import com.example.filesystemtask.models.FileCounter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FileCountRepositoryImpl implements FileCountRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public FileCountRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Adds FileCounter object to Database.
     *
     * @param fileCounter FileCounter object
     * @return Nothing
     */
    @Override
    public void addFileCount(FileCounter fileCounter) {
        try (Session session = sessionFactory.openSession()) {
            session.save(fileCounter);
        }
    }

    @Override
    public int getLastFileCount() {
        try(Session session = sessionFactory.openSession()) {
            Query<FileCounter> query = session.createQuery(
                    "from FileCounter ", FileCounter.class);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("No files in the system.");
            }

            List<FileCounter> factors = query.list();
            return factors.get(factors.size()-1).getFileId();

        } catch (Exception e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }
}
