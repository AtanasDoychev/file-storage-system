package com.example.filesystemtask.repositories;

import com.example.filesystemtask.exceptions.FileOperationsException;
import com.example.filesystemtask.repositories.contracts.StorageRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Repository
public class StorageRepositoryImpl implements StorageRepository {

    /**
     * Transfers file of type 'MultipartFile' to target directory.
     *
     * @param multipartFile
     * @param destinationDirectory
     * @param newFileName
     * @throws FileOperationsException if file fails to transfer
     * @return Nothing
     */
    @Override
    public void storeFileToDirectory(MultipartFile multipartFile,
                                     String destinationDirectory,
                                     String newFileName){

        try {
            multipartFile.transferTo(new File(
                    destinationDirectory + "\\" + newFileName));
        } catch (IOException e){
            throw new FileOperationsException(e.getMessage());
        }

    }

    /**
     * Deletes a file by Path provided.
     *
     * @param path of file to delete
     * @throws FileOperationsException if file is not deleted
     * @return Nothing
     */
    @Override
    public void deleteFileOrDirectory(Path path){

        try {
            Files.delete(path);
        } catch (IOException e){
            throw new FileOperationsException(e.getMessage());
        }
    }
}
