package com.example.filesystemtask.repositories;

import com.example.filesystemtask.exceptions.EntityNotFoundException;
import com.example.filesystemtask.models.SystemState;
import com.example.filesystemtask.repositories.contracts.SystemStateRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.filesystemtask.constants.GlobalConstants.*;

@Repository
public class SystemStateRepositoryImpl implements SystemStateRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public SystemStateRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addSystemState(SystemState systemState) {
        try (Session session = sessionFactory.openSession()) {
            session.save(systemState);
        }
    }

    @Override
    public SystemState getSystemState() {
        try(Session session = sessionFactory.openSession()) {
            Query<SystemState> query = session.createQuery(
                    "from SystemState", SystemState.class);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(NO_SYSTEM_STATES);
            }
            List<SystemState> systemStates = query.list();
            // under we get the last object of the list
            return systemStates.get(systemStates.size() - 1);
        } catch (Exception e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public int getSystemCapacity() {
        try(Session session = sessionFactory.openSession()) {
            Query<SystemState> query = session.createQuery(
                    "from SystemState", SystemState.class);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(NO_SYSTEM_STATES);
            }
            //todo how to optimize the query not to get all states from DB, but the last only
            List<SystemState> systemStates = query.list();
            // under we get the last object of the list
            return systemStates.get(systemStates.size() - 1).getTreeCapacity();
        } catch (Exception e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public int getSystemDirectoryCount() {
        try(Session session = sessionFactory.openSession()) {
            Query<SystemState> query = session.createQuery(
                    "from SystemState", SystemState.class);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(NO_SYSTEM_STATES);
            }
            //todo how to optimize the query not to get all states from DB, but the last only
            List<SystemState> systemStates = query.list();
            SystemState lastState = systemStates.get(systemStates.size() - 1);
            //divide tree capacity by dir. capacity and get dir. count
            return lastState.getTreeCapacity() / lastState.getDirCapacity();

        } catch (Exception e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public int getDirectoryCapacity() {
        try(Session session = sessionFactory.openSession()) {
            Query<SystemState> query = session.createQuery(
                    "from SystemState", SystemState.class);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(NO_SYSTEM_STATES);
            }
            //todo how to optimize the query not to get all states from DB, but the last only
            List<SystemState> systemStates = query.list();
            SystemState lastState = systemStates.get(systemStates.size() - 1);
            //divide tree capacity by dir. capacity and get dir. count
            return lastState.getDirCapacity();

        } catch (Exception e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }
}
