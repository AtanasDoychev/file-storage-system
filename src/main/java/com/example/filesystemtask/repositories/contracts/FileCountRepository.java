package com.example.filesystemtask.repositories.contracts;

import com.example.filesystemtask.models.FileCounter;


public interface FileCountRepository {

    /**
     * Adds FileCounter object to Database.
     *
     * @param fileCounter FileCounter object
     * @return Nothing
     */
    void addFileCount(FileCounter fileCounter);

    int getLastFileCount();
}
