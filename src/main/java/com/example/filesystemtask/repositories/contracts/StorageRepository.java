package com.example.filesystemtask.repositories.contracts;

import com.example.filesystemtask.exceptions.FileOperationsException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;

public interface StorageRepository {

    /**
     * Transfers file of type 'MultipartFile' to target directory.
     *
     * @param multipartFile
     * @param destinationDirectory
     * @param newFileName
     * @throws FileOperationsException if file fails to transfer
     * @return Nothing
     */
    void storeFileToDirectory(MultipartFile multipartFile,
                              String destinationDirectory,
                              String newFileName) throws IOException;

    /**
     * Deletes a file by Path provided.
     *
     * @param path of file to delete
     * @throws FileOperationsException if file is not deleted
     * @return Nothing
     */
    void deleteFileOrDirectory(Path path) throws IOException;
}
