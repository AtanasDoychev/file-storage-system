package com.example.filesystemtask.repositories.contracts;

import com.example.filesystemtask.models.SystemState;

public interface SystemStateRepository {

    void addSystemState(SystemState systemState);

    int getSystemCapacity();

    SystemState getSystemState();

    int getSystemDirectoryCount();

    int getDirectoryCapacity();
}
