package com.example.filesystemtask.services;

import org.springframework.web.multipart.MultipartFile;

public interface FileStorageService {

    /**
     * This is the main method responsible for handling uploaded files.
     * Every file receives new name with ID attached in front.
     * Next is a check, if the file ID is higher number than system's
     * capacity and if it is, the tree directory structure capacity is increased.
     * After that the file is processed and saved.
     *
     * @param multipartFile <p>
     * The file to be saved.
     * @return String with file's new name, including file ID.
     *
     * @author Atanas Doychev
     * @version 1.0
     * @since   2021-01-05
     */
    String storeFile(MultipartFile multipartFile);

    /**
     * Builds a url string of the tree, to the file we need by file ID. <p>
     * Used for searching by ID and downloading by ID. <p>
     * It gets all files from the directory where the wanted file is <p>
     * and then searches trough them until it finds the needed.
     *
     * @param fileId path to the root  <p>
     * @return String of the path to the wanted file
     */
    String getFilePathById(int fileId);

}
