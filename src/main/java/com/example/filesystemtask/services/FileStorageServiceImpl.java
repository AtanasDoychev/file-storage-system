package com.example.filesystemtask.services;

import com.example.filesystemtask.exceptions.EntityNotFoundException;
import com.example.filesystemtask.repositories.contracts.FileCountRepository;
import com.example.filesystemtask.repositories.contracts.SystemStateRepository;
import com.example.filesystemtask.services.factories.contracts.DirectoryStructuresFactory;
import com.example.filesystemtask.services.factories.contracts.FileFactory;
import com.example.filesystemtask.services.operations.contracts.DirectoryTreeOperations;
import com.example.filesystemtask.services.operations.contracts.FileOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

import static com.example.filesystemtask.constants.GlobalConstants.TREE_DIRECTORY;

@Service
public class FileStorageServiceImpl implements FileStorageService {

    private final FileFactory fileFactory;
    private final FileOperations fileOperations;
    private final FileCountRepository fileCountRepository;
    private final SystemStateRepository systemStateRepository;
    private final DirectoryStructuresFactory directoryStructuresFactory;
    private final DirectoryTreeOperations directoryTreeOperations;


    @Autowired
    public FileStorageServiceImpl(FileFactory fileFactory,
                                  FileOperations fileOperations,
                                  FileCountRepository fileCountRepository,
                                  SystemStateRepository systemStateRepository,
                                  DirectoryStructuresFactory directoryStructuresFactory,
                                  DirectoryTreeOperations directoryTreeOperations) {
        this.fileFactory = fileFactory;
        this.fileOperations = fileOperations;
        this.fileCountRepository = fileCountRepository;
        this.systemStateRepository = systemStateRepository;
        this.directoryStructuresFactory = directoryStructuresFactory;
        this.directoryTreeOperations = directoryTreeOperations;
    }

    /**
     * This is the main method responsible for handling uploaded files.
     * Every file receives new name with ID attached in front.
     * Next is a check, if the file ID is higher number than system's
     * capacity and if it is, the tree directory structure capacity is increased.
     * After that the file is processed and saved.
     *
     * @param multipartFile <p>
     * The file to be saved.
     * @return String with file's new name, including file ID.
     *
     * @author Atanas Doychev
     * @version 1.0
     * @since   2021-01-05
     */
    @Override
    public String storeFile(MultipartFile multipartFile) {

        String newFileName = fileFactory.buildNewUniqueFileName(multipartFile);

        int newFileId = fileFactory.getFileId(newFileName);

        int currentSystemCapacity = systemStateRepository.getSystemCapacity();

        if (newFileId > currentSystemCapacity){
            directoryStructuresFactory.rebuildTreeStructure();
        }

        fileOperations.processFile(multipartFile, newFileName);

        return newFileName;
    }

    /**
     * Builds a url string of the tree, to the file we need by file ID. <p>
     * Used for searching by ID and downloading by ID. <p>
     * It gets all files from the directory where the wanted file is <p>
     * and then searches trough them until it finds the needed.
     *
     * @param fileId path to the root  <p>
     * @return String of the path to the wanted file
     */
    @Override
    public String getFilePathById(int fileId) {

        int fileCount = fileCountRepository.getLastFileCount();

        if (fileId > fileCount){
            throw new EntityNotFoundException(
                    String.format("There's no file with ID: %d", fileId));
        }

        String out = "";

        int directoryCount = systemStateRepository.getSystemDirectoryCount();

        int directoryId = directoryTreeOperations.getDirectoryIdByFileId(fileId);

        String directoryPathWithFile = fileOperations.buildDirPathByDirId(
                TREE_DIRECTORY, 1, directoryCount + 1, directoryId);

        File[] filesInDirectory = new File(directoryPathWithFile).listFiles(File::isFile);

        for (int i = 0; i < filesInDirectory.length; i++) {
            File currentFile = filesInDirectory[i];
            int currentFileId = fileFactory.getFileId(currentFile.getName());

            if (currentFileId == fileId){
                out = currentFile.getAbsolutePath();
                break;
            }

        }

        return out;
    }
}
