package com.example.filesystemtask.services;

import com.example.filesystemtask.models.SystemState;
import com.example.filesystemtask.models.dto.CalculateSystemStateDto;

public interface SystemStateService {

    /**
     * Calculates system capacity by given proportion ratio and directory capacity.
     *
     * @param systemStateDto <p>
     * @return String of calculations
     */
    String calculate(CalculateSystemStateDto systemStateDto);

    /**
     * Changes the state of the system. <p>
     * 1. Adds the new state to the database. <p>
     * 2. Rebuilds the tree accordingly to the new state. <p>
     *
     * @param newSystemState <p>
     * @return nothing
     */
    void changeState(SystemState newSystemState);

}
