package com.example.filesystemtask.services;

import com.example.filesystemtask.models.SystemState;
import com.example.filesystemtask.models.dto.CalculateSystemStateDto;
import com.example.filesystemtask.repositories.contracts.SystemStateRepository;
import com.example.filesystemtask.services.factories.contracts.DirectoryStructuresFactory;
import org.springframework.stereotype.Service;

@Service
public class SystemStateServiceImpl implements SystemStateService {

    private final SystemStateRepository systemStateRepository;
    private final DirectoryStructuresFactory directoryStructuresFactory;

    public SystemStateServiceImpl(SystemStateRepository systemStateRepository,
                                  DirectoryStructuresFactory directoryStructuresFactory) {
        this.systemStateRepository = systemStateRepository;
        this.directoryStructuresFactory = directoryStructuresFactory;
    }

    /**
     * Calculates system capacity by given proportion ratio and directory capacity.
     *
     * @param dto <p>
     * @return String of calculations
     */
    @Override
    public String calculate(CalculateSystemStateDto dto) {
        StringBuffer buffer = new StringBuffer();

        long systemCap = (dto.getSystemProportion() * dto.getDirectoryCapacity()) * dto.getDirectoryCapacity();
        long dirCount = dto.getSystemProportion() * dto.getDirectoryCapacity();

        buffer.append(String.format("Potential system setup%n%n%n"));

        buffer.append(String.format("Capacity: %d%n%n", systemCap));

        buffer.append(String.format("Proportion: %d:1%n%n", dto.getSystemProportion()));

        buffer.append(String.format("Directory count: %d%n%n", dirCount));

        buffer.append(String.format("Directory capacity: %d%n%n", dto.getDirectoryCapacity()));

        return buffer.toString();
    }

    /**
     * Changes the state of the system. <p>
     * When the directory structure is the same as the old, <p>
     * only state is updated, tree is not rebuilt. <p>
     *
     * @param newSystemState <p>
     * @return nothing
     */
    @Override
    public void changeState(SystemState newSystemState) {
        SystemState currentState = systemStateRepository.getSystemState();

        //if the same tree structure, do not re-build, only add new factors or dir state
        if (currentState.getTreeCapacity() == newSystemState.getTreeCapacity() &&
                currentState.getProportionSystem() == newSystemState.getProportionSystem() &&
                currentState.getDirCapacity() == newSystemState.getDirCapacity()) {

            systemStateRepository.addSystemState(newSystemState);
        } else {
            systemStateRepository.addSystemState(newSystemState);
            directoryStructuresFactory.rebuildTreeStructure();
        }
    }
}
