package com.example.filesystemtask.services.factories;

import com.example.filesystemtask.services.factories.contracts.BalancedBinarySearchTreeFactory;
import com.example.filesystemtask.services.factories.contracts.DirectoryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.example.filesystemtask.constants.GlobalConstants.*;

@Service
public class BalancedBinarySearchTreeFactoryImpl implements BalancedBinarySearchTreeFactory {

    private final DirectoryFactory directoryFactory;

    @Autowired
    public BalancedBinarySearchTreeFactoryImpl(DirectoryFactory directoryFactory) {
        this.directoryFactory = directoryFactory;
    }


    /**
     * This method builds a binary, balanced, search tree
     * structure of directories from a given capacity.<p>
     * Each directory has an ID number, as one
     * of the numbers from 1 to the given capacity.
     *
     * @param directoryCount <p>
     * Defines the number of <p>
     * directories to be build.<p>
     * @return Nothing
     *
     * @author Atanas Doychev
     * @version 1.1
     * @since   2021-01-05
     */
    @Override
    public void buildBalancedBinarySearchTreeDirectoryStructure(int directoryCount) {

        List<Integer> ascendingOrderNumbers = getAscendingNumbersFromZeroTo(directoryCount);

        List<String> searchTreeDirectories =
                buildDirectoryStructureStrings(ascendingOrderNumbers, "");

        Set<Path> directoryPathsToExecute =
                buildDirectoryStructurePaths(searchTreeDirectories, TREE_DIRECTORY);

        directoryFactory.buildDirectoriesFromPaths(directoryPathsToExecute);

    }

    /**
     * This method creates an incrementing list of integers <p>
     * from 1 to given capacity factor. <p>
     *
     * @param directoryCount
     * @return List<Integer>
     */
    private List<Integer> getAscendingNumbersFromZeroTo(int directoryCount){

        List<Integer> ascendingOrderNumbers = new ArrayList<>();

        for (int i = 1; i <= directoryCount; i++) {
            ascendingOrderNumbers.add(i);
        }
        return ascendingOrderNumbers;
    }

    /**
     * Recursive method building binary, balanced, <p>
     * search tree directory structure paths, <p>
     * without the actual URl of the root directory <p>
     * where the structure will be build. <p>
     *
     * @param straightNumbers a sequence of numbers, <p>
     *                        representing the directory IDs. <p>
     * @param path starts from empty string and increases <p>
     *             its length with recursive cycle. <p>
     * @return List<Strings>
     */
    private List<String> buildDirectoryStructureStrings(List<Integer> straightNumbers, String path){
        StringBuilder builder = new StringBuilder();
        List<String> searchTreeStrings = new ArrayList<>();

        //bottom
        if (straightNumbers.size() == 0){
            searchTreeStrings.add(path);
            return searchTreeStrings;
        }

        //here directories receive ID
        int midNum = straightNumbers.get(straightNumbers.size()/2);
        builder.append(path + midNum + BACKSLASH);

        //get left side
        List<Integer> left = new ArrayList<>();
        for (int i = 0; i < straightNumbers.size()/2; i++) {
            left.add(straightNumbers.get(i));
        }

        //get right side
        List<Integer> right = new ArrayList<>();
        for (int i = (straightNumbers.size()/2) + 1; i < straightNumbers.size(); i++) {
            right.add(straightNumbers.get(i));
        }

        List<String> tempLeft = buildDirectoryStructureStrings(left, builder.toString());
        searchTreeStrings.addAll(tempLeft);

        List<String> tempRight = buildDirectoryStructureStrings(right, builder.toString());
        searchTreeStrings.addAll(tempRight);

        return searchTreeStrings;
    }


    /**
     * Add URL to a set of directory paths.
     *
     * @param paths list of directory pats
     * @param url a URL of the tree root directory
     * @return Set<Path>
     */
    private Set<Path> buildDirectoryStructurePaths(List<String> paths, String url){

        Set<Path> directoryPaths = new HashSet<>();

        for (String path : paths) {
            directoryPaths.add(Path.of(url + path));
        }

        return directoryPaths;
    }
}
