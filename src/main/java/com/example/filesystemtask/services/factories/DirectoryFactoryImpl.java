package com.example.filesystemtask.services.factories;

import com.example.filesystemtask.exceptions.DirectoryBuildException;
import com.example.filesystemtask.services.factories.contracts.DirectoryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;

@Service
public class DirectoryFactoryImpl implements DirectoryFactory {

    @Autowired
    public DirectoryFactoryImpl() {
    }

    /**
     * Creates directories from list of Paths.
     *
     * @param paths Set of Path <p>
     * @throws DirectoryBuildException <p>
     * In case directories fail to build.
     * @return Nothing
     */
    @Override
    public void buildDirectoriesFromPaths(Set<Path> paths)  {

        for (Path path : paths) {
            try{
                Files.createDirectories(path);
            } catch (IOException e){
                throw new DirectoryBuildException(e.getMessage());
            }
        }
    }

    /**
     * Creates single directory from a string URL,
     * if it does not exists already.
     *
     * @param path String of URL <p>
     * @return Nothing
     */
    @Override
    public void buildDirectoryIfDoesNotExist(String path){

        File currentDirectory = new File(path);

        if (!currentDirectory.exists()) {
            currentDirectory.mkdirs();
        }
    }
}
