package com.example.filesystemtask.services.factories;

import com.example.filesystemtask.exceptions.FileOperationsException;
import com.example.filesystemtask.models.SystemState;
import com.example.filesystemtask.repositories.contracts.FileCountRepository;
import com.example.filesystemtask.repositories.contracts.SystemStateRepository;
import com.example.filesystemtask.services.factories.contracts.BalancedBinarySearchTreeFactory;
import com.example.filesystemtask.services.factories.contracts.DirectoryFactory;
import com.example.filesystemtask.services.factories.contracts.DirectoryStructuresFactory;
import com.example.filesystemtask.services.factories.contracts.FileFactory;
import com.example.filesystemtask.services.operations.contracts.DirectoryTreeOperations;
import com.example.filesystemtask.services.operations.contracts.FileOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;

import static com.example.filesystemtask.constants.GlobalConstants.*;

@Service
public class DirectoryStructuresFactoryImpl implements DirectoryStructuresFactory {

    private final FileFactory fileFactory;
    private final FileOperations fileOperations;
    private final DirectoryFactory directoryFactory;
    private final FileCountRepository fileCountRepository;
    private final SystemStateRepository systemStateRepository;
    private final DirectoryTreeOperations directoryTreeOperations;
    private final BalancedBinarySearchTreeFactory balancedBinarySearchTreeFactory;

    @Autowired
    public DirectoryStructuresFactoryImpl(FileFactory fileFactory,
                                          FileOperations fileOperations,
                                          DirectoryFactory directoryFactory,
                                          FileCountRepository fileCountRepository,
                                          SystemStateRepository systemStateRepository,
                                          DirectoryTreeOperations directoryTreeOperations,
                                          BalancedBinarySearchTreeFactory balancedBinarySearchTreeFactory) {
        this.fileFactory = fileFactory;
        this.fileOperations = fileOperations;
        this.directoryFactory = directoryFactory;
        this.fileCountRepository = fileCountRepository;
        this.systemStateRepository = systemStateRepository;
        this.directoryTreeOperations = directoryTreeOperations;
        this.balancedBinarySearchTreeFactory = balancedBinarySearchTreeFactory;
    }

    /**
     * Executing on startup, this method builds the initial
     * directory structures for this file system. <p>
     * 1. 'File-system' directory <p>
     * 2. 'Archive' sub-directory <p>
     * 3. 'Extensions' sub-directory <p>
     * 4. 'Tree' root sub-directory with initial capacity and IDs.<p>
     *
     * @return Nothing
     */
    @EventListener(ApplicationReadyEvent.class)
    public void buildInitialDirectoryStructure() {

        File root = new File(DIRECTORY_CONTAINER);
        if (!root.exists()) {
            SystemState initialSystemState = createInitialSystemStateObject();
            addSystemStateToDatabase(initialSystemState);

            directoryFactory.buildDirectoryIfDoesNotExist(TREE_DIRECTORY);
            directoryFactory.buildDirectoryIfDoesNotExist(ARCHIVE_DIRECTORY);
            directoryFactory.buildDirectoryIfDoesNotExist(EXTENSIONS_DIRECTORY);

            int directoryCount = initialSystemState.getTreeCapacity() / initialSystemState.getDirCapacity();

            balancedBinarySearchTreeFactory
                    .buildBalancedBinarySearchTreeDirectoryStructure(directoryCount);

        }
    }

    /**
     * 1. Create new tree directory structure. <p>
     * 2. Copy files from archive to new structure. <p>
     * 3. Delete old tree structure. <p>
     *
     * @return Nothing
     */
    @Override
    public void rebuildTreeStructure() {

        int fileCount = fileCountRepository.getLastFileCount();
        int systemCapacity = systemStateRepository.getSystemCapacity();
        //if the tree is rebuilding automatically when capacity full
        //have to increase the tree capacity
        //else use the last system state - its due to rearranging the tree
        if (fileCount - 1 == systemCapacity) {
            buildNewTreeStructureAndIncreaseCapacity();
        } else {
            buildNewTreeStructure();
        }

        File archiveDirectory = new File(ARCHIVE_DIRECTORY);
        File treeDirectory = new File(TREE_DIRECTORY);

        copyFilesFromArchiveToNewTree(archiveDirectory);

        deleteOldTreeDirectory(archiveDirectory, treeDirectory);
    }

    /**
     * Used to create new tree structure in cases
     * when the structure is changed by user,
     * before it reaches its file capacity.
     *
     * @return nothing
     */
    private void buildNewTreeStructure() {

        int directoryCount = systemStateRepository.getSystemDirectoryCount();

        balancedBinarySearchTreeFactory.buildBalancedBinarySearchTreeDirectoryStructure(directoryCount);
    }

    /**
     * Builds new tree directory structure with increased capacity.Used when auto rebuild.<p>
     * 1. Increase capacity based on the old capacity factor. <p>
     * 2. Build the directory tree structure. <p>
     * 3. Add capacity factor to database. <p>
     *
     * @return Nothing
     */
    private void buildNewTreeStructureAndIncreaseCapacity() {

        SystemState currentSystemState = systemStateRepository.getSystemState();

        //if true dir is Fixed
        //else - NOT fixed
        if (currentSystemState.isDirState()) {
            buildTreeAndIncCapWhenDirCapFixed(currentSystemState);
        } else {
            buildTreeAndIncCapWhenDirCapNotFixed(currentSystemState);
        }

    }

    /**
     * Builds a new tree structure and increases its capacity. <p>
     * This method is used when the tree is updating automatically. <p>
     * Also, only in situations when the directory growth factor is FIXED. <p>
     * And finally adds new directory state to database.
     *
     * @param currentSystemState
     * @return Nothing
     */
    private void buildTreeAndIncCapWhenDirCapFixed(SystemState currentSystemState) {

        SystemState newSystemState = buildNewSystemStateDirCapFixed(currentSystemState);

        int newDirectoryCount = newSystemState.getProportionSystem() * newSystemState.getDirCapacity();

        balancedBinarySearchTreeFactory.buildBalancedBinarySearchTreeDirectoryStructure(newDirectoryCount);

        systemStateRepository.addSystemState(newSystemState);
    }

    /**
     * Creates new system state object for the cases <p>
     * when directory growth factor is FIXED.
     *
     * @param currentSystemState
     * @return SystemState
     */
    private SystemState buildNewSystemStateDirCapFixed(SystemState currentSystemState) {
        //when fixed we just increase the directory count and proportion
        SystemState newSystemState = new SystemState();

        //increasing the proportions, since the directory file cap. is fixed
        newSystemState.setProportionSystem(
                currentSystemState.getProportionSystem() + currentSystemState.getProportionGrowthFactor());

        //new proportion times folder cap. gives directory count
        //directory count times director cap. gives total system file cap.
        newSystemState.setTreeCapacity(
                (newSystemState.getProportionSystem() * currentSystemState.getDirCapacity())
                        * currentSystemState.getDirCapacity());

        //this one stays the same
        newSystemState.setProportionDir(currentSystemState.getProportionDir());

        //proportion growth factor stays the same
        newSystemState.setProportionGrowthFactor(currentSystemState.getProportionGrowthFactor());

        //directory capacity stays the same, it is FIXED
        newSystemState.setDirCapacity(currentSystemState.getDirCapacity());

        //growth factor stays the same
        newSystemState.setDirGrowthFactor(currentSystemState.getDirGrowthFactor());

        //directory state stays the same
        newSystemState.setDirState(currentSystemState.isDirState());

        //get the time of creation
        newSystemState.setDateTime(LocalDateTime.now());

        return newSystemState;
    }

    /**
     * Builds a new tree structure and increases its capacity. <p>
     * This method is used when the tree is updating automatically. <p>
     * Also, only in situations when the directory growth factor is NOT FIXED. <p>
     * And finally adds new directory state to database.
     *
     * @param currentSystemState
     * @return Nothing
     */
    private void buildTreeAndIncCapWhenDirCapNotFixed(SystemState currentSystemState) {

        SystemState newSystemState = buildNewSystemStateDirCapNotFixed(currentSystemState);

        int newDirectoryCount = newSystemState.getProportionSystem() * newSystemState.getDirCapacity();

        balancedBinarySearchTreeFactory.buildBalancedBinarySearchTreeDirectoryStructure(newDirectoryCount);

        systemStateRepository.addSystemState(newSystemState);
    }

    /**
     * Creates new system state object for the cases <p>
     * when directory growth factor is NOT FIXED.
     *
     * @param currentSystemState
     * @return SystemState
     */
    private SystemState buildNewSystemStateDirCapNotFixed(SystemState currentSystemState) {
        SystemState newSystemState = new SystemState();

        //increase directory capacity by growth factor, dir.cap NOT FIXED
        newSystemState.setDirCapacity(
                currentSystemState.getDirCapacity() + currentSystemState.getDirGrowthFactor());


        //proportion increases by adding proportion growth factor
        newSystemState.setProportionSystem(
                currentSystemState.getProportionSystem() + currentSystemState.getProportionGrowthFactor());

        //new tree capacity - (new dir.cap * system.proportion) * dir.cap
        newSystemState.setTreeCapacity((newSystemState.getDirCapacity() *
                newSystemState.getProportionSystem()) * newSystemState.getDirCapacity());

        //we never change that, stays the same
        newSystemState.setProportionDir(currentSystemState.getProportionDir());

        //stays the same
        newSystemState.setProportionGrowthFactor(currentSystemState.getProportionGrowthFactor());

        //stays the same
        newSystemState.setDirGrowthFactor(currentSystemState.getDirGrowthFactor());

        //stays the same
        newSystemState.setDirState(currentSystemState.isDirState());

        //current date time
        newSystemState.setDateTime(LocalDateTime.now());

        return newSystemState;
    }


    /**
     * Copy all files from normal directory to directory
     * with binary balanced search tree structure.<p>
     * This method uses the files IDs to locate the directories
     * in the tree structure where files should be copied.<p>
     *
     * @param archiveDirectory
     * @return Nothing
     */
    //keeping this private, because it is a big copy procedure
    private void copyFilesFromArchiveToNewTree(File archiveDirectory) {

        File[] archiveFiles = new File(archiveDirectory.getPath()).listFiles(File::isFile);

        int directoryCount = systemStateRepository.getSystemDirectoryCount();

        if (archiveFiles != null) {
            for (File file : archiveFiles) {
                int fileId = fileFactory.getFileId(file.getName());
                int directoryId = directoryTreeOperations.getDirectoryIdByFileId(fileId);

                //this under works directly with the NEW system state
                //and always gets the latest directory count
                //which means it builds the right url to the needed directory
                //without specifying the new tree structure
                String destinationDirectory = fileOperations.
                        buildDirPathByDirId(TREE_DIRECTORY, 1, directoryCount + 1, directoryId);

                copyFileFromDirToDir(file, destinationDirectory);
            }
        }
    }


    /**
     * Makes a copy of a single file from one directory to another.<p>
     *
     * @param fileToMove
     * @param destinationDirectory
     * @return Nothing
     * @throws FileOperationsException if copying fails.
     */
    //this copy procedure stays private too
    private void copyFileFromDirToDir(File fileToMove, String destinationDirectory) {

        Path source = Paths.get(fileToMove.getAbsolutePath());
        Path target = Paths.get(destinationDirectory + "\\" + fileToMove.getName());

        try {
            Files.copy(source, target, StandardCopyOption.COPY_ATTRIBUTES);
        } catch (IOException e) {
            throw new FileOperationsException(e.getMessage());
        }

    }

    /**
     * Checks in the new tree structure contains the same count <p>
     * of files as the archive and then deletes the old tree structure.
     *
     * @param archiveDirectory
     * @param treeDirectory
     * @return Nothing
     */
    //delete procedure should be private too
    private void deleteOldTreeDirectory(File archiveDirectory, File treeDirectory) {

        //this all could be optimized to delete the old tree directly
        int archiveFilesCount = fileOperations.getFileCountOfDirectoryTree(archiveDirectory);
        File[] treeRootDirectories = new File(treeDirectory.getPath()).listFiles(File::isDirectory);
        File newTree = directoryTreeOperations.getTheNewTree(treeRootDirectories);
        int newTreeFileCount = fileOperations.getFileCountOfDirectoryTree(newTree);
        File oldTree = directoryTreeOperations.getTheOldTree(treeRootDirectories);

        //check if new tree contains as much files as the archive, then delete old tree structure
        if (archiveFilesCount == newTreeFileCount) {
            deleteDirectoryAndFilesInIt(oldTree);
        }
    }

    /**
     * Recursive method deleting the directory given and everything inside it.
     *
     * @param directoryToDelete
     * @return Nothing
     */
    private void deleteDirectoryAndFilesInIt(File directoryToDelete) {

        File[] files = new File(directoryToDelete.getPath()).listFiles(File::isFile);
        if (files != null) {
            for (File fileToDelete : files) {
                fileToDelete.delete();
            }
        }

        File[] subDirectories = new File(directoryToDelete.getPath()).listFiles(File::isDirectory);

        if (subDirectories != null) {
            for (File subDirectory : subDirectories) {
                deleteDirectoryAndFilesInIt(subDirectory);
            }
        }

        directoryToDelete.delete();
    }

    /**
     * Add SystemState objects to database.
     *
     * @param systemState
     * @return nothing
     */
    private void addSystemStateToDatabase(SystemState systemState) {
        systemStateRepository.addSystemState(systemState);
    }

    /**
     * Creates the initial SystemState.
     * Used once when the program is started.
     *
     * @return SystemState
     */
    private SystemState createInitialSystemStateObject() {
        SystemState initialSystemState = new SystemState();
        initialSystemState.setTreeCapacity(INITIAL_TREE_CAPACITY);
        initialSystemState.setProportionSystem(PROPORTION_SYSTEM);
        initialSystemState.setProportionDir(PROPORTION_DIRECTORY);
        initialSystemState.setProportionGrowthFactor(PROPORTION_GROWTH_FACTOR);
        initialSystemState.setDirCapacity(DIRECTORY_CAPACITY);
        initialSystemState.setDirGrowthFactor(DIRECTORY_CAPACITY_GROWTH_FACTOR);
        initialSystemState.setDirState(DIRECTORY_STATE_FIXED);
        initialSystemState.setDateTime(DATE_OF_CREATION);
        return initialSystemState;
    }
}
