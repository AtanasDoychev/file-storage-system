package com.example.filesystemtask.services.factories;

import com.example.filesystemtask.constants.GlobalConstants;
import com.example.filesystemtask.repositories.contracts.FileCountRepository;
import com.example.filesystemtask.services.factories.contracts.FileFactory;
import com.example.filesystemtask.models.FileCounter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileFactoryImpl implements FileFactory {

    private final FileCountRepository fileCountRepository;

    @Autowired
    public FileFactoryImpl(FileCountRepository fileCountRepository) {
        this.fileCountRepository = fileCountRepository;
    }

    /**
     * Builds new unique file name by adding ID at front, surrounded by open and close tags.<p>
     * Example for file with ID 4: <p>
     * From: "FileName.txt" <p>
     * To: "#_4_#_FileName.txt" <p>
     *
     * @param multipartFile
     * @return String with new file name
     */
    @Override
    public String buildNewUniqueFileName(MultipartFile multipartFile){

        String newFileId = addFileCountToDataBase();

        return (GlobalConstants.OPEN_TAG + newFileId + GlobalConstants.CLOSE_TAG + multipartFile.getOriginalFilename());
    }

    /**
     * Extracts an file extension from a file name.
     * Runs backwards.
     *
     * @param fileName
     * @return String with extension
     */
    @Override
    public String extractExtension(String fileName){
        StringBuilder builder = new StringBuilder();

        for (int i = fileName.length()-1; i >= 0; i--) {
            if (fileName.charAt(i) == GlobalConstants.DOT){
                break;
            }else {
                builder.append(fileName.charAt(i));
            }
        }

        return builder.reverse().toString();
    }

    /**
     * Extracts ID number from a file name by traversing forward. <p>
     * This method works only for ID surrounded by specific tags. <p>
     * Example: \tree\#_3_#_fileName <p>
     * When open tag #_ is recognized extraction begins
     * until close tag _#_ is recognized. <p>
     *
     * @param fileName
     * @return int with file ID
     */
    @Override
    public int getFileId(String fileName){
        StringBuilder builder = new StringBuilder();
        boolean extract = false;

        for (int i = 0; i < fileName.length(); i++) {

            //start id extraction
            if (fileName.charAt(i) == GlobalConstants.HASHTAG &&
                    fileName.charAt(i + 1) == GlobalConstants.UNDER_LINE){
                extract = true;
                i++;
                continue;
            }

            //end id extraction
            if (fileName.charAt(i) == GlobalConstants.UNDER_LINE &&
                    fileName.charAt(i + 1) == GlobalConstants.HASHTAG &&
                    fileName.charAt(i + 2) == GlobalConstants.UNDER_LINE){
                break;
            }

            //id extraction
            if (extract){
                int currentCharAscii = fileName.charAt(i);
                //check if digit
                if (currentCharAscii >= 48 && currentCharAscii <= 57){
                    builder.append(fileName.charAt(i));
                } else {
                    throw new IllegalArgumentException(GlobalConstants.CONTAINS_NON_DIGIT_SYMBOLS);
                }
            }
        }

        return Integer.parseInt(builder.toString());
    }


    /**
     * Registers auto-incremental file ID to database.
     *
     * @return String with incremented file ID
     */
    private String addFileCountToDataBase(){
        FileCounter counter = new FileCounter();
        fileCountRepository.addFileCount(counter);
        return String.valueOf(counter.getFileId());
    }
}
