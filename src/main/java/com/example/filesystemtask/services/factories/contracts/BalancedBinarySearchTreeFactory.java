package com.example.filesystemtask.services.factories.contracts;

public interface BalancedBinarySearchTreeFactory {

    /**
     * This method builds a binary, balanced, search tree
     * structure of directories from a given capacity.<p>
     * Each directory has an ID number, as one
     * of the numbers from 1 to the given capacity.
     *
     * @param directoryCount <p>
     * Defines the number of <p>
     * directories to be build.<p>
     * @return Nothing
     *
     * @author Atanas Doychev
     * @version 1.1
     * @since   2021-01-05
     */
    void buildBalancedBinarySearchTreeDirectoryStructure(int directoryCount);
}
