package com.example.filesystemtask.services.factories.contracts;

import com.example.filesystemtask.exceptions.DirectoryBuildException;

import java.nio.file.Path;
import java.util.Set;

public interface DirectoryFactory {

    /**
     * Creates directories from list of Paths.
     *
     * @param paths Set of Path <p>
     * @throws DirectoryBuildException <p>
     * In case directories fail to build.
     * @return Nothing
     */
    void buildDirectoriesFromPaths(Set<Path> paths);

    /**
     * Creates single directory from a string URL,
     * if it does not exists already.
     *
     * @param path String of URL <p>
     * @return Nothing
     */
    void buildDirectoryIfDoesNotExist(String path);

}
