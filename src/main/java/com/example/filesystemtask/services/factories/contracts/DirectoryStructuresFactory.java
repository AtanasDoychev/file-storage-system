package com.example.filesystemtask.services.factories.contracts;

public interface DirectoryStructuresFactory {

    /**
     * 1. Create new tree directory structure. <p>
     * 2. Copy files from archive to new structure. <p>
     * 3. Delete old tree structure. <p>
     *
     * @return Nothing
     */
    void rebuildTreeStructure();

}
