package com.example.filesystemtask.services.factories.contracts;

import org.springframework.web.multipart.MultipartFile;

public interface FileFactory {

    /**
     * Builds new unique file name by adding ID at front, surrounded by open and close tags.<p>
     * Example for file with ID 4: <p>
     * From: "FileName.txt" <p>
     * To: "#_4_#_FileName.txt" <p>
     *
     * @param multipartFile
     * @return String with new file name
     */
    String buildNewUniqueFileName(MultipartFile multipartFile);

    /**
     * Extracts an file extension from a file name.
     * Runs backwards.
     *
     * @param fileName
     * @return String with extension
     */
    String extractExtension(String fileName);

    /**
     * Extracts ID number from a file name by traversing forward. <p>
     * This method works only for ID surrounded by specific tags. <p>
     * Example: \tree\#_3_#_fileName <p>
     * When open tag #_ is recognized extraction begins
     * until close tag _#_ is recognized. <p>
     *
     * @param fileName
     * @return int with file ID
     */
    int getFileId(String fileName);
}
