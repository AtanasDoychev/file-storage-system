package com.example.filesystemtask.services.operations;

import com.example.filesystemtask.repositories.contracts.SystemStateRepository;
import com.example.filesystemtask.services.operations.contracts.DirectoryTreeOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class DirectoryTreeOperationsImpl implements DirectoryTreeOperations {

    private final SystemStateRepository systemStateRepository;

    @Autowired
    public DirectoryTreeOperationsImpl(SystemStateRepository systemStateRepository) {
        this.systemStateRepository = systemStateRepository;
    }

    /**
     * Returns the newer structure of two trees, <p>
     * by comparing their actual directory counts <p>
     * and directory calculation from the last system state. <p>
     *
     * @param treeRootSubDirectories <p>
     * @return File
     */
    @Override
    public File getTheNewTree(File[] treeRootSubDirectories) {
        // +1 for the root directory
        int firstTreeDirCount = getSubDirectoriesCount(treeRootSubDirectories[0]) + 1;
        int currentSystemDirCount = systemStateRepository.getSystemDirectoryCount();

        if (firstTreeDirCount == currentSystemDirCount) {
            return treeRootSubDirectories[0];
        } else {
            return treeRootSubDirectories[1];
        }
    }

    /**
     * Returns the older structure of two trees, <p>
     * by comparing their actual directory counts <p>
     * and directory calculation from the last system state. <p>
     *
     * @param treeRootSubDirectories <p>
     * @return File
     */
    @Override
    public File getTheOldTree(File[] treeRootSubDirectories) {
        // +1 for the root directory
        int firstTreeDirCount = getSubDirectoriesCount(treeRootSubDirectories[0]) + 1;
        int currentSystemDirCount = systemStateRepository.getSystemDirectoryCount();

        if (firstTreeDirCount == currentSystemDirCount) {
            return treeRootSubDirectories[1];
        } else {
            return treeRootSubDirectories[0];
        }
    }

    /**
     * Calculates the ID of a directory, by given file ID. <p>
     * Used when we need to know in which directory a file belongs <p>
     * accordingly to the directory capacity. <p>
     *
     * @param fileId <p>
     * @return int
     */
    @Override
    public int getDirectoryIdByFileId(int fileId) {
        double dirCapacity = systemStateRepository.getDirectoryCapacity();
        double result = fileId / dirCapacity;

        if (result % 1 == 0) {
            return (int) result;
        } else {
            //+1 because it rounds it down and we need the number above
            return (int) result + 1;
        }
    }

    /**
     * Traversing trough binary balanced search tree structure of directories.
     * The purpose of this method is to count the sub-directories a tree
     * structure contains. <p>
     *
     * @param directory root of the structure
     * @return int of sub-directories count
     */
    private int getSubDirectoriesCount(File directory) {
        int count = 0;

        File[] treeDirectories = new File(directory.getPath()).listFiles(File::isDirectory);

        //if directory has sub directories continue, if not - bottom - return zero
        if (treeDirectories != null) {
            count += treeDirectories.length;
            for (File subDirectory : treeDirectories) {
                int subCount = getSubDirectoriesCount(subDirectory);
                count += subCount;
            }
        }
        return count;
    }
}
