package com.example.filesystemtask.services.operations;

import com.example.filesystemtask.constants.GlobalConstants;
import com.example.filesystemtask.exceptions.FileOperationsException;
import com.example.filesystemtask.repositories.contracts.StorageRepository;
import com.example.filesystemtask.repositories.contracts.SystemStateRepository;
import com.example.filesystemtask.services.factories.contracts.DirectoryFactory;
import com.example.filesystemtask.services.factories.contracts.FileFactory;
import com.example.filesystemtask.services.operations.contracts.DirectoryTreeOperations;
import com.example.filesystemtask.services.operations.contracts.FileOperations;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import static com.example.filesystemtask.constants.GlobalConstants.TREE_DIRECTORY;

@Service
public class FileOperationsImpl implements FileOperations {

    private final FileFactory fileFactory;
    private final DirectoryFactory directoryFactory;
    private final StorageRepository storageRepository;
    private final DirectoryTreeOperations directoryTreeOperations;
    private final SystemStateRepository systemStateRepository;

    public FileOperationsImpl(FileFactory fileFactory,
                              DirectoryFactory directoryFactory,
                              StorageRepository storageRepository,
                              DirectoryTreeOperations directoryTreeOperations,
                              SystemStateRepository systemStateRepository) {
        this.fileFactory = fileFactory;
        this.directoryFactory = directoryFactory;
        this.storageRepository = storageRepository;
        this.directoryTreeOperations = directoryTreeOperations;
        this.systemStateRepository = systemStateRepository;
    }

    /**
     * Processing file to directories.<p>
     * Designed to store the file in one most important directory,
     * and then copy the stored file to other directories.<p>
     * 1. Transfer file to Archive directory.<p>
     * 2. Copy file from Archive to Extensions.<p>
     * 3. Copy file from Archive to Tree.<p>
     * <b>Note</b> Storage directories are previously declared in
     * 'GlobalConstants.java' class.<p>
     *
     * @param multipartFile file to store
     * @param newFileName   new file name
     * @return Nothing
     */
    @Override
    public void processFile(MultipartFile multipartFile, String newFileName) {

        try {
            storageRepository.storeFileToDirectory(multipartFile, GlobalConstants.ARCHIVE_DIRECTORY, newFileName);
        } catch (IOException e) {
            throw new FileOperationsException(e.getMessage());
        }

        copyFileFromArchiveToExtensionsDirectory(newFileName);

        copyFileFromArchiveToTreeDirectory(newFileName);
    }

    /**
     * Copy file from 'Archive' to 'Extensions', directory.
     * This method works with the file name, it extracts file's extension,
     * and if a directory of this extension type is not present in 'Extensions',
     * will be created and file copied afterwords. <p>
     *
     * @param newFileName new file name
     * @return Nothing
     */
    private void copyFileFromArchiveToExtensionsDirectory(String newFileName) {

        Path source = Paths.get(GlobalConstants.ARCHIVE_DIRECTORY + newFileName);

        String fileExtension = fileFactory.extractExtension(newFileName);
        String targetDirectory = GlobalConstants.EXTENSIONS_DIRECTORY + fileExtension;
        directoryFactory.buildDirectoryIfDoesNotExist(targetDirectory);

        Path target = Paths.get(targetDirectory + GlobalConstants.BACKSLASH + newFileName);

        try {
            Files.copy(source, target, StandardCopyOption.COPY_ATTRIBUTES);
        } catch (IOException e) {
            throw new FileOperationsException(e.getMessage());
        }
    }

    /**
     * Copy file from 'Archive' to 'Tree', directory.
     * This method works with the file name, it extracts file's ID,
     * and then provides target directory path based on file ID.
     * Finally copy the file.<p>
     *
     * @param newFileName new file name
     * @return Nothing
     */
    private void copyFileFromArchiveToTreeDirectory(String newFileName) {

        Path source = Paths.get(GlobalConstants.ARCHIVE_DIRECTORY + newFileName);

        int fileId = fileFactory.getFileId(newFileName);
        int directoryId = directoryTreeOperations.getDirectoryIdByFileId(fileId);
        int directoryCount = systemStateRepository.getSystemDirectoryCount();

        String targetDirectoryPath = buildDirPathByDirId(TREE_DIRECTORY, 1, directoryCount + 1, directoryId);

        Path target = Paths.get(targetDirectoryPath + GlobalConstants.BACKSLASH + newFileName);

        try {
            Files.copy(source, target, StandardCopyOption.COPY_ATTRIBUTES);
        } catch (IOException e) {
            throw new FileOperationsException(e.getMessage());
        }
    }

    /**
     * Counts all non-directory files inside a tree directory structure.
     * This is recursive method, and the count includes
     * all files inside all sub-directories of the given directory.
     *
     * @param directory root directory file
     * @return int of file count
     */
    @Override
    public int getFileCountOfDirectoryTree(File directory) {

        int totalFileCount = 0;

        File[] files = new File(directory.getPath()).listFiles(File::isFile);
        if (files != null) {
            totalFileCount += files.length;
        }

        File[] subDirectories = new File(directory.getPath()).listFiles(File::isDirectory);

        if (subDirectories != null) {
            for (File subDirectory : subDirectories) {
                int subFileCount = getFileCountOfDirectoryTree(subDirectory);
                totalFileCount += subFileCount;
            }
        }

        return totalFileCount;
    }

    /**
     * Builds a path/url to a concrete directory by its ID. <p>
     * It is used when search and copy files from/in tree structure. <p>
     *
     * Tested up to 15,000 folders and it works as expected left and right the tree.
     *
     * @param path - path to the root  <p>
     * @param min - always starts as 1 <p>
     * @param max - always directory count + 1 <p>
     * @param dirId - actual directory ID <p>
     * @return String of the path to the wanted directory
     */
    @Override
    public String buildDirPathByDirId(String path, int min, int max, int dirId) {

        String out = "";

        int midNumber = ((max - min) / 2) + min;

        //if this true we have found/build the directory path we need
        if (dirId == midNumber){
            return String.format("%s%s\\", path, midNumber);
        }

        String pathBuild = String.format("%s%s\\", path, midNumber);

        //go left, if not go right
        if (dirId < midNumber){
            String temp = buildDirPathByDirId(pathBuild, min, midNumber, dirId);
            out += temp;
        }else {
            String temp = buildDirPathByDirId(pathBuild, midNumber+1, max, dirId);
            out += temp;
        }

        return out;
    }
}
