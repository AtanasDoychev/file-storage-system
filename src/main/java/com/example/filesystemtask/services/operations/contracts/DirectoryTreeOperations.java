package com.example.filesystemtask.services.operations.contracts;

import java.io.File;

public interface DirectoryTreeOperations {

    /**
     * Returns the newer structure of two trees, <p>
     * by comparing their actual directory counts <p>
     * and directory calculation from the last system state. <p>
     *
     * @param treeRootSubDirectories <p>
     * @return File
     */
    File getTheNewTree(File[] treeRootSubDirectories);

    /**
     * Returns the older structure of two trees, <p>
     * by comparing their actual directory counts <p>
     * and directory calculation from the last system state. <p>
     *
     * @param treeRootSubDirectories <p>
     * @return File
     */
    File getTheOldTree(File[] treeRootSubDirectories);

    /**
     * Calculates the ID of a directory, by given file ID. <p>
     * Used when we need to know in which directory a file belongs <p>
     * accordingly to the directory capacity. <p>
     *
     * @param fileId <p>
     * @return int
     */
    int getDirectoryIdByFileId(int fileId);
}
