package com.example.filesystemtask.services.operations.contracts;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public interface FileOperations {

    /**
     * Processing file to directories.<p>
     * Designed to store the file in one most important directory,
     * and then copy the stored file to other directories.<p>
     * 1. Transfer file to Archive directory.<p>
     * 2. Copy file from Archive to Extensions.<p>
     * 3. Copy file from Archive to Tree.<p>
     * <b>Note</b> Storage directories are previously declared in
     * 'GlobalConstants.java' class.<p>
     *
     * @param multipartFile file to store
     * @param newFileName new file name
     *
     * @return Nothing
     */
    void processFile(MultipartFile multipartFile, String newFileName);

    /**
     * Counts all non-directory files inside a tree directory structure.
     * This is recursive method, and the count includes
     * all files inside all sub-directories of the given directory.
     *
     * @param directory root directory file
     *
     * @return int of file count
     */
    int getFileCountOfDirectoryTree(File directory);

    /**
     * Builds a path/url to a concrete directory by its ID. <p>
     * It is used when search and copy files from/in tree structure. <p>
     *
     * Tested up to 15,000 folders and it works as expected left and right the tree.
     *
     * @param path - path to the root  <p>
     * @param min - always starts as 1 <p>
     * @param max - always directory count + 1 <p>
     * @param dirId - actual directory ID <p>
     * @return String of the path to the wanted directory
     */
    String buildDirPathByDirId(String path, int min, int max, int dirId);

}
